<?php


namespace GivxCustom;

class GravityForms {

	private $tab_index;

	public function init () {

		add_filter( 'gform_confirmation_anchor', [$this, 'anchorTrue'] );

		add_filter( 'gform_tabindex', [$this, 'tabIndex'] );
	}

	public function anchorTrue () {
		return true;
	}

	function tabIndex () {
		$this->tab_index += 20;
		return $this->tab_index;
	}

}