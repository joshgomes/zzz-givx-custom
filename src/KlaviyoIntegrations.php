<?php

namespace GivxCustom;

use BnfoKlaviyo\KlaviyoException;
use BnfoKlaviyo\ServerApi;

class KlaviyoIntegrations {

	private $form;

	private $entry;

	private $customer_properties;

	public function init() {

        add_action( 'gform_after_submission_4', [ $this, 'givxReportForm' ], 10, 2 );
		add_action( 'gform_after_submission_3', [ $this, 'givxHomepageForm' ], 10, 2 );
		add_action( 'gform_after_submission_1', [ $this, 'givxHomepageForm' ], 10, 2 );

	}

	/**
	 * @param $entry
	 * @param $form
	 *
	 * @throws KlaviyoException
	 *
	 */
	public function givxHomepageForm( $entry, $form ) {

        $this->prepareCustomerProperties($entry, $form);

		// Set Division
		$properties['Method'] ='GivX Homepage';

		$klaviyo_api = new ServerApi();

		$klaviyo_api->track( 'Registered GivX Interest', $this->customer_properties, $properties );

	}

    /**
     * @param $entry
     * @param $form
     *
     * @throws KlaviyoException
     *
     */
    public function givxReportForm( $entry, $form ) {

        $this->prepareCustomerProperties($entry, $form);

        // Set Division
        $properties['Download'] ='GivX Report 2018';

        $klaviyo_api = new ServerApi();

        $klaviyo_api->track( 'Downloaded GivX Content', $this->customer_properties, $properties );

    }

	private function prepareCustomerProperties($entry, $form) {

        $this->form = $form;

        $this->entry = $entry;

        $first_name_key = array_search( 'First Name', array_column( $this->form['fields'], 'label' ) );
        $first_name_id  = $this->form['fields'][ $first_name_key ]['id'];

        $last_name_key = array_search( 'Last Name', array_column( $this->form['fields'], 'label' ) );
        $last_name_id  = $this->form['fields'][ $last_name_key ]['id'];

        $email_address_key = array_search( 'Email Address', array_column( $this->form['fields'], 'label' ) );
        $email_address_id  = $this->form['fields'][ $email_address_key ]['id'];

        $company_key = array_search( 'Company', array_column( $this->form['fields'], 'label' ) );
        $company_id  = $this->form['fields'][ $company_key ]['id'];

        $phone_key = array_search( 'Phone', array_column( $this->form['fields'], 'label' ) );
        $phone_id  = $this->form['fields'][ $phone_key ]['id'];

        $this->customer_properties = [
            '$email' => rgar($this->entry, $email_address_id),
            '$first_name' => rgar($this->entry, $first_name_id),
            '$last_name' => rgar($this->entry, $last_name_id),
            '$organization' => rgar($this->entry, $company_id),
            '$phone_number' => rgar($this->entry, $phone_id)
        ];
    }


}