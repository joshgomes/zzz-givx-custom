<?php

/**
 * Plugin Name: GivX Custom
 * Plugin URI: https://benefacto.org
 * Description: A home for custom classes and functionality for the GivX Website
 * Version: 3.2.2
 * Author: Linz Darlington & Josh Gomes, Team Benefacto
 * Author URI: https://benefacto.org
 */

namespace GivxCustom;

use BnfoPsrFour;

class GivXCustom{

	public function init() {

		add_action( 'plugins_loaded', [ $this, 'autoloader' ] );
		add_action( 'plugins_loaded', [ $this, 'initialiseClasses' ] );
		add_action( 'admin_print_styles', [ $this, 'enqueue_local_admin_assets' ], 1 );
		add_action( 'admin_print_styles', [ $this, 'enqueue_other_admin_assets' ], 1 );


	}

	public function autoloader() {

        if ( ! is_plugin_active( 'zza-bnfo-psrfour/Autoloader.php' ) ) {
            return;
        }

		$auto_loader = new BnfoPsrFour\Autoloader();

		$auto_loader->addNamespace( 'GivxCustom', dirname( __FILE__ ) . '/src' );

		$auto_loader->register();

	}

	public function initialiseClasses() {

		$gravity_forms = new GravityForms();
		$gravity_forms->init();

		$klaviyo_integrations = new KlaviyoIntegrations();
		$klaviyo_integrations->init();

	}

	/**
	 * Enqueue the new custom style sheet for admin purposes and the bootstrap grid only
	 */


	function enqueue_local_admin_assets() {


	}


	function enqueue_other_admin_assets() {


	}


}


$bnfo_admin = new GivXCustom();
$bnfo_admin->init();